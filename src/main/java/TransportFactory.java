import transport.*;

public class TransportFactory {

    public Transport getTransport(TransportType transportType) {
        if(transportType.equals(TransportType.AIRPLANE)) {
            return new Airplane();
        } else if(transportType.equals(TransportType.SHIP)) {
            return new Ship();
        } else if(transportType.equals(TransportType.TRUCK)) {
            return new Truck();
        } else if(transportType.equals(TransportType.SPACESHIP)) {
            return new Spaceship();
        } else {
            return null;
        }
    }
}
