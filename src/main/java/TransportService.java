import transport.Ship;
import transport.Transport;
import transport.Truck;

public class TransportService {

   /* public void shipMethod(Ship ship){
        System.out.println("Rapunzel is ready for boarding: " + ship.deliver());
    }

    public void truckMethod(Truck truck){
        System.out.println("Shrek is ready to drive: " + truck.deliver());
    }*/

    public void shipOrTruckMethod(Ship ship, Truck truck){
        if(ship != null){
            System.out.println("Rapunzel is ready for boarding: " + ship.deliver());
        } else if (truck != null){
            System.out.println("Shrek is ready to drive: " + truck.deliver());
        } else {
            System.out.println("Rapunzel and shrek are so sad. :((");
        }
    }

    public void transportMethod(Transport transport){
        if(transport == null){
            System.out.println("Transport object cannot be found ");
        } else{
            System.out.println("Here is your transport object: " + transport.deliver());
        }
    }
}
