package transport;

public class Ship implements Transport {

    @Override
    public String deliver() {
        return "Ship objects loaded.";
    }
}
