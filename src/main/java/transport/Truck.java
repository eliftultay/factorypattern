package transport;

public class Truck implements Transport {
    @Override
    public String deliver() {
        return "Truck objects loaded.";
    }
}
