package transport;

public interface Transport {
    public String deliver();
}
