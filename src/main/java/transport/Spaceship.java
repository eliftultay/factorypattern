package transport;

public class Spaceship implements Transport{
    @Override
    public String deliver() {
        return "Space mining tools loaded.";
    }
}
