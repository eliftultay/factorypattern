import transport.Transport;
/*
* This class depends on only the TransportFactory and Transport classes resulting in higher
* decoupling which is good news.
* This class has only one responsibility(SRP), that is using transport objects without creating them.
 * */
public class WithFactory {
    public void executeExample() {
        TransportFactory transportFactory = new TransportFactory();
        TransportService transportService = new TransportService();

        Transport airplane = transportFactory.getTransport(TransportType.AIRPLANE);
        transportService.transportMethod(airplane);

        Transport ship = transportFactory.getTransport(TransportType.SHIP);
        transportService.transportMethod(ship);

        Transport truck = transportFactory.getTransport(TransportType.TRUCK);
        transportService.transportMethod(truck);

        Transport spaceship = transportFactory.getTransport(TransportType.SPACESHIP);
        transportService.transportMethod(spaceship);
    }
}
