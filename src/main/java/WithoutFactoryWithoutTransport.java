import transport.Airplane;
import transport.Ship;
import transport.Truck;
/*
* We cannot use shipOrTruckMethod of class TransportService with Airplane parameter
* because we need to modify the method to accept an Airplane parameter.
* We also need to add an if statement inside the method.
* This class depends on Ship, Truck, Airplane and Spaceship classes and
* maybe  more, in case there are new transport types(ie. Subway).
* Any changes to  those classes could potentially cause an error on this class.
* Additionally, this class has multiple responsibilities. The first one is
* creating Transport objects, the second one is using them which is against
* Single Responsibility Principle (SRP).
* */

public class WithoutFactoryWithoutTransport {
    public void executeExample() {
        TransportService transportService = new TransportService();

        Ship ship = new Ship();
        transportService.shipOrTruckMethod(ship,null);

        Truck truck = new Truck();
        transportService.shipOrTruckMethod(null, truck);

        Airplane airplane = new Airplane();
    }
}
