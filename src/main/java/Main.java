public class Main {
    public static void main (String [] args){
        WithoutFactoryWithoutTransport withoutFactoryWithoutTransport =
                new WithoutFactoryWithoutTransport();
        System.out.println("WithoutFactoryWithoutTransport:");
        withoutFactoryWithoutTransport.executeExample();

        WithoutFactoryWithTransport withoutFactoryWithTransport =
                new WithoutFactoryWithTransport();
        System.out.println("WithoutFactoryWithTransport:");
        withoutFactoryWithTransport.executeExample();

        WithFactory withFactory = new WithFactory();
        System.out.println("WithFactory:");
        withFactory.executeExample();
    }

}