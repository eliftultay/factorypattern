import transport.*;
/*
 * Compared to WithoutFactoryWithoutTransport example, a simplified and generic
 * version of shipOrTruckMethod method(transportMethod) is implemented.
 * This class depends on Ship, Truck, Airplane and Spaceship classes and
 * maybe  more, in case there are new transport types(ie. Subway).
 * Any changes to  those classes could potentially cause an error on this class.
 * Additionally, this class has multiple responsibilities. The first one is
 * creating Transport objects, the second one is using them which is against
 * Single Responsibility Principle (SRP).
 * */
public class WithoutFactoryWithTransport {
    public void executeExample() {
        TransportService transportService = new TransportService();

        Transport ship = new Ship();
        transportService.transportMethod(ship);

        Transport truck = new Truck();
        transportService.transportMethod(truck);

        Transport airplane = new Airplane();
        transportService.transportMethod(airplane);

        Transport spaceship = new Spaceship();
        transportService.transportMethod(spaceship);
    }
}
